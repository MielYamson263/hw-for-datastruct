import datetime
import time
import sys
dates = []
Entries = []
def List():
    print("These are your Entries..")
    for i in range(0, len(Entries)): 
        print("Entry Number:", i,dates[i])
    print()
    Menu()
def date():
    x = (datetime.datetime.now())
    y = dates.append(x)
    print(x)
def Menu():    
    print("Welcome to your personal journal!")
    action = int(input("Type 1 to Add an Entry\nType 2 to Read an Entry\nType 3 to Remove an Entry\nType 4 to List all Entries\nType 5 to Exit\n"))
    if(action==1):
        Add()
    elif(action==2):
        Read()
    elif(action==3):
        Remove()
    elif(action==4):
        List()
    elif(action==5):
        print("CY@")
        time.sleep(1)
        sys.exit()
    else:
        time.sleep(1)
        print("Invalid Input")
        Menu()
def Add():
    Entry = input("Write your entry: ")
    Entries.append(Entry)
    print("Entry Added")
    print()
    date()
    Menu()  
def Read():
        r = input("Read Entry No.:\n")
        if r not in Entries or r is str:
            print("Entry not found..")
            Menu()
        else:
            print(Entries[r])
            print()
            Menu()  
    
def Remove():
    try:
        d = int(input("Delete Entry No.:\n"))
        del Entries[d]
        del dates[d]
        print("Entry Removed")
        print()
        Menu()  
    except:
        print("Entry does not exist")
        time.sleep(1)
        Menu()
Menu()
